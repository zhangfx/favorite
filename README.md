# 网址收藏

#### 项目介绍
网址收藏

##Vue相关
1. Vue.js  
	https://cn.vuejs.org/v2/guide/
2. elementui  
    https://element.faas.ele.me/#/zh-CN/component/installation
3. vue-element-admin github  
    https://github.com/PanJiaChen/vue-element-admin
4. vue-element-admin 文档  
    https://panjiachen.github.io/vue-element-admin-site/#/zh-cn/README
5. vue撸后台系列文档  
	https://segmentfault.com/a/1190000009275424

##Spring Cloud相关
1. Spring Cloud Doc  
    http://cloud.spring.io/spring-cloud-static/Finchley.RELEASE/single/spring-cloud.html
2. 史上最简单的 SpringCloud 教程  
    https://blog.csdn.net/forezp/article/details/70148833
3. 纯洁的微笑 SpringCloud示例  
    https://gitee.com/ityouknow/spring-cloud-examples
4. Spring Cloud Config的综合管理后台  
    https://github.com/dyc87112/spring-cloud-config-admin/tree/master
    https://github.com/stone-jin/spring-cloud-config-admin-doc/blob/master/index.md

## SpringBoot 中文索引  
    http://springboot.fun
## Spring Cloud 中文索引
    http://springcloud.fun
## 纯洁的微笑博客  
    http://www.ityouknow.com/



## CentOS 安全防护
### 防护措施
1. 安装Fail2ban：限制同一IP登录测试，防止暴力破解登录用户密码
2. 修改ssh端口


### 防护参考网站
1. CentOS 7服务器安全配置  
    https://blog.csdn.net/zosoyi/article/details/78468710

    
## CentOS 环境安装
### 设置开机自启动   
    /etc/rc.local
### 安装软件
1. 安装jdk
2. 安装openresty
3. 安装redis
4. 安装mysql
5. 安装docker

### 参考网站
1. CentOS7 linux下yum安装redis以及使用
    https://www.cnblogs.com/rslai/p/8249812.html
2. centos7下使用yum安装mysql
    https://www.cnblogs.com/julyme/p/5969626.html
3. Centos7上安装docker
    https://www.cnblogs.com/yufeng218/p/8370670.html
